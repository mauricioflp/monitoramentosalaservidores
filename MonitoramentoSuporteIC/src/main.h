#ifndef __MAIN_H__
#define __MAIN_H__

#include <Arduino.h>

typedef enum TStatusLed { INICIANDO, DISCONECTADO, FIM_INICIALIZACAO, CONECTADO, LENDO_SENSOR, ENVIANDO_INFO, ERRO_MQTT, OK_MQTT, DESLIGAR_LED } TStatusLed;

const int PINO_VERMELHO = D1;
const int PINO_VERDE = D2;
const int PINO_AZUL = D3;

const int PINO_TEMP1 = D4;

const int TAM_MEDIA = 15;

#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE DHT11
//#define DHTPIN  D4
//const uint8_t DHTPIN = D4; 
const uint8_t DHTPIN = 5; 

const int EEPROM_ID = 0;
const int EEPROM_SSID = 250;
const int EEPROM_PASS = 100; 

typedef struct TCor {
    int R, G, B;
} TCor;

const TCor Vermelho = {1,0,0};
const TCor Verde = {0,1,0};
const TCor Azul = {0,0, 1};
const TCor Amarelo = {0,1,1};
const TCor Magenta = {1,1,0};
const TCor Ciano = {1,0,1};
const TCor Branco = {1,1,1};
const TCor Desligado = {0,0,0};




/*********************************************************************************************
 *  Rotina para mostrar a porta serial funcionando e 
 *  as informações do software para o usuario
*********************************************************************************************/
void MsgInicializacao();

/*********************************************************************************************
 * Rotina de conexão e status da conexão
*********************************************************************************************/
void ConectarWiFi();

/*************************************************************************************
 * Mostra as configurações de rede, da placa e qualidade de sinal
 *************************************************************************************/
void PrintWiFiConfigs();

/*************************************************************************************
 * Mostra na serial os comandos disponveis
 *************************************************************************************/
void help();

/*************************************************************************************
 * Funcao para obter os parametros passados pela serial
 *************************************************************************************/
String obterParametroSerial(int nroParam );

/*************************************************************************************
 * Tratamento de mensagens vindas da porta serial
 *************************************************************************************/
void tratarEntradaSerial();

/*************************************************************************************
 * Funçao para tratar os dados recebidos pela porta serial
 *************************************************************************************/
void serialEvent() ;

/*************************************************************************************
 *  Callback para chegada de mensagens do broker
 *************************************************************************************/
void mqtt_callback(char* topic, byte* payload, unsigned int length);

/*************************************************************************************
 *  A chegada de msg MQTT é tratada para o firmware saber o que precisa ser feito
 *************************************************************************************/
void TratarMsgRecebida(String topic, String msg );

/*************************************************************************************
 * Inicializa conexão com MQTT
 *************************************************************************************/
void ConectarMQTT();

/********************************************************************************************
 *  Ajusta o led RGB para a cor selecionada
 ********************************************************************************************/
void SetLed( TCor c );

/**
 * @brief Ajusta a barra de status do oled
 * 
 */
void SetBarraStatus (int );

/********************************************************************************************
 *  Usa o Led para informar os usuários a situação atual
 ********************************************************************************************/
void ReportarInfoUsuario( TStatusLed status );

/********************************************************************************************
 *  Inicializando o sensor DS18B20
 ********************************************************************************************/
bool ConectarSensor();

/********************************************************************************************
 *  Lendo dado de temperatura do sensor
 ********************************************************************************************/
float LerInfoSensor();

/**
 * @brief Inicializando o sensor DHT22
 * 
 */
bool ConectarSensorDHT22();

/********************************************************************************************
 *  Lendo dado de temperatura do sensor DHT22
 ********************************************************************************************/
void LerTempSensorDHT22( float *, float *);


/********************************************************************************************
 *  Salvando configurações na EEPROM
 ********************************************************************************************/
void SaveConfigEEPROM( String key, String value);

/**
 * @brief Função de Gravação de configurações
 * 
 */
void LoadConfigsEEPROM();

/**
 * @brief Função para calcular a média das últimas 15 leituras
 * 
 */
float CalculaMedia( float v );
#endif