#include <Arduino.h>
#include "ESP8266WiFi.h"
//#include <WiFi.h>
//#include "WiFiEsp.h"
#include "secrets.h"
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <SSD1306Wire.h>
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

#include <EEPROM.h>

#include "main.h"

#define logo48_width 48
#define logo48_height 48
static unsigned char logo48_bits[] = {
   0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x00,
   0xfe, 0x31, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x20, 0x00, 0x00, 0x00, 0x00,
   0x06, 0x60, 0x00, 0x00, 0x00, 0x00, 0xf6, 0x30, 0x00, 0x00, 0x00, 0x00,
   0xf6, 0x3b, 0x00, 0x00, 0x00, 0x00, 0x36, 0x1f, 0x00, 0x00, 0x00, 0x00,
   0x36, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x36, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x7f, 0x00, 0xfc, 0x0f, 0x00,
   0x36, 0x7f, 0x00, 0xff, 0x3f, 0x00, 0x36, 0x7f, 0x80, 0xff, 0x3f, 0x00,
   0x36, 0x7f, 0xe0, 0xff, 0x1f, 0x00, 0x36, 0x7f, 0xe0, 0xff, 0x0f, 0x00,
   0x36, 0x7f, 0xf0, 0xff, 0x0f, 0x00, 0x36, 0x7f, 0xf8, 0xff, 0x07, 0x00,
   0x36, 0x7f, 0xf8, 0x0f, 0x04, 0x1f, 0x36, 0x7f, 0xfc, 0x03, 0x80, 0x1b,
   0x36, 0x7f, 0xfc, 0xf1, 0xc0, 0x10, 0x36, 0x7f, 0xfc, 0xf9, 0x73, 0x12,
   0x36, 0x7f, 0xfe, 0x1d, 0xbf, 0x13, 0x36, 0x7f, 0xfe, 0x0c, 0xc0, 0x13,
   0x36, 0x7f, 0xfe, 0x0c, 0xfe, 0x12, 0x36, 0x7f, 0xfe, 0x1d, 0x3f, 0x12,
   0x36, 0x7f, 0xfc, 0xf9, 0x01, 0x12, 0x36, 0x7f, 0xfc, 0xf1, 0x00, 0x12,
   0x36, 0x7f, 0xfc, 0x03, 0x00, 0x12, 0x36, 0x7f, 0xf8, 0x07, 0x00, 0x12,
   0x36, 0x7f, 0xf8, 0x1f, 0x06, 0x12, 0x36, 0x7f, 0xf0, 0xff, 0x0f, 0x12,
   0x36, 0x7f, 0xf0, 0xff, 0x0f, 0x12, 0x36, 0x7f, 0xe0, 0xff, 0x1f, 0x12,
   0x36, 0x7f, 0xc0, 0xff, 0x3f, 0x12, 0x36, 0x7f, 0x00, 0xff, 0x3f, 0x12,
   0x36, 0x7f, 0x00, 0xfe, 0x0f, 0x12, 0x36, 0x26, 0x00, 0xe0, 0x01, 0x12,
   0x66, 0x00, 0x78, 0x00, 0x00, 0x12, 0xcc, 0x00, 0xfe, 0x00, 0x00, 0x13,
   0x98, 0xff, 0xc7, 0xff, 0xff, 0x13, 0x30, 0x00, 0x00, 0x00, 0x00, 0x10,
   0xe0, 0x00, 0x00, 0x00, 0x00, 0x18, 0xc0, 0xff, 0xc7, 0xff, 0xff, 0x1f,
   0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };


#define logoCAP48_width 48
#define logoCAP48_height 48
static unsigned char logoCAP48_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x81, 0x00, 0x00,
   0x00, 0x00, 0xe3, 0xc7, 0x01, 0x00, 0x00, 0x80, 0xf7, 0xef, 0x03, 0x00,
   0x00, 0x80, 0x7f, 0xfe, 0x01, 0x00, 0x00, 0x00, 0x3f, 0xfc, 0x00, 0x00,
   0x00, 0x00, 0x1f, 0x78, 0x00, 0x00, 0x00, 0x08, 0x0f, 0xf0, 0x38, 0x00,
   0x00, 0x9c, 0x07, 0xe0, 0x7d, 0x00, 0x00, 0xfe, 0x03, 0xc0, 0x3f, 0x00,
   0x00, 0xfc, 0x01, 0x80, 0x1f, 0x00, 0x00, 0xf8, 0x00, 0x00, 0x0f, 0x00,
   0x40, 0x78, 0x00, 0x00, 0x1e, 0x06, 0xe0, 0x3c, 0x00, 0x00, 0x3c, 0x0f,
   0xf0, 0x1f, 0x00, 0x00, 0xf8, 0x0f, 0xe0, 0x0f, 0x00, 0x00, 0xf0, 0x07,
   0xc0, 0x07, 0x00, 0x00, 0xe0, 0x03, 0xc0, 0x03, 0x00, 0x00, 0xc0, 0x03,
   0xe0, 0x01, 0x00, 0x00, 0x80, 0x07, 0xf0, 0x00, 0x84, 0x21, 0x03, 0x0f,
   0x70, 0x40, 0x80, 0x21, 0x02, 0x0e, 0x38, 0x60, 0x40, 0x21, 0x03, 0x1c,
   0x38, 0x60, 0x00, 0x23, 0x00, 0x1c, 0x70, 0x60, 0x00, 0x22, 0x00, 0x0e,
   0xf0, 0xc0, 0x24, 0x26, 0x00, 0x0f, 0xe0, 0x01, 0x00, 0x00, 0x80, 0x07,
   0xc0, 0x03, 0x00, 0x00, 0xc0, 0x03, 0xe0, 0x07, 0x00, 0x00, 0xe0, 0x07,
   0xf0, 0x0f, 0x00, 0x00, 0xf0, 0x0f, 0xf8, 0x1e, 0x00, 0x00, 0xf8, 0x0f,
   0x70, 0x3c, 0x00, 0x00, 0x3c, 0x0e, 0x00, 0x78, 0x00, 0x00, 0x1e, 0x04,
   0x00, 0xfc, 0x00, 0x00, 0x1f, 0x00, 0x00, 0xfc, 0x01, 0x80, 0x3f, 0x00,
   0x00, 0xfe, 0x03, 0xc0, 0x7f, 0x00, 0x00, 0x9e, 0x07, 0xe0, 0x79, 0x00,
   0x00, 0x04, 0x0f, 0xf0, 0x30, 0x00, 0x00, 0x00, 0x1f, 0xf8, 0x00, 0x00,
   0x00, 0x80, 0x3f, 0xfc, 0x01, 0x00, 0x00, 0xc0, 0x7f, 0xfe, 0x03, 0x00,
   0x00, 0xc0, 0xf3, 0xcf, 0x03, 0x00, 0x00, 0x80, 0xe1, 0x87, 0x01, 0x00,
   0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };



  
// Criação objeto display
SSD1306Wire display(0x3c, 2, 14);

CmdParser cmdParser;
CmdBuffer<64> cmdBuffer;


DHT_Unified dht(DHTPIN, DHTTYPE);
//Variáveis e objetos globais
WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient);
String topicoConectadoTemp ;
String topicoConectadoUmid;
String topicoTemperatura ;
String topicoUmidade;
//String topicoConectado = "IC_Sala" + String(ID_MQTT) + "_online";
//String topicoTemperatura = "IC_Sala" + String(ID_MQTT) + "_Temp1";

// variavel de aviso do momento de ler dados de temperatura;
bool flagSensorTemp1 ;
OneWire sensorTemp1(PINO_TEMP1);
float TemperaturaAtual;
float UmidadeAtual;
float MediaAtual ;
float vetorMedia[TAM_MEDIA];
unsigned long qtdeItensMedia=0;
/**
 * @brief Variável para controle se 15 amostras foram lidas
 * 
 */
bool flagTamanhoMedia = false;

// ajusto do tipo de sensor (configurado na inicialização )
byte type_s;
// vetor de bytes para detecção e configuração do tipo de sensor de temperatura
byte addr[8];


#ifndef HAVE_HWSERIAL1
//#include "SoftwareSerial.h"
// Serial1(6, 7); // RX, TX
#endif

const char Versao[] = "Versão 2.1 (Setembro/2020)";
const char Firmware[] = "Sistema de monitoramento de ambiente (Sala de Servidores)";
const char Separador[] = "*************************************************************";
// armazena a situação de rede
int statusRede = WL_IDLE_STATUS; 
bool pisca = false;
String bufferSerial;

long instanteUltimoEnvio = 0;  // TimeStamp da ultima mensagem enviada
const long TempoEspera = 5000;
int intervalo = 10000;   

/**
 * @brief Variável que armazena o SSID da rede wifi
 * 
 */
String ssid;
/**
 * @brief Variável que armazena a senha associada ao ssid
 * 
 */
String pass;

//SSD1306Wire display(0x3c, 2, 14);


/********************************************************************************************
 *  Ajusta o led RGB para a cor selecionada
 ********************************************************************************************/
void SetLed( TCor c )
{
  digitalWrite(PINO_VERMELHO, c.R) ;
  digitalWrite(PINO_VERDE, c.G) ;
  digitalWrite(PINO_AZUL, c.B) ;
}

#define GREEN           0x07E0
void SetBarraStatus( int porc )
{
    display.drawProgressBar(14,1,100, 14, porc );
    
}

/********************************************************************************************
 *  Usa o Led para informar os usuários a situação atual
 ********************************************************************************************/
void ReportarInfoUsuario( TStatusLed status )
{
  switch (status)
  {
  case INICIANDO : 
  //Apaga o display
  display.clear();
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 2, "Instituto de Computação");  
  display.drawXbm(40,16,48,48,logo48_bits);
  display.display();
  delay(6000);

  display.clear();
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 2, "CAP - IC - UFMT");  
  display.drawXbm(40,16,48,48,logoCAP48_bits);
  display.display();  
  delay(4000);  
  display.clear();
    //Atualiza informacoes de inicialização
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 38, F("Iniciando")); 
  display.drawString(64, 50, F("Aguardando serial"));
  SetBarraStatus(20);
  
  break;
    
  case DISCONECTADO:  
    //Apaga o display
    display.clear();    
    SetBarraStatus(40);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Tentando conectar Wi-Fi");     
    delay(1000); 
    break;

  case CONECTADO:  
    display.clear();
    SetBarraStatus(60);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Conectado a rede ");      
    delay(500);
    break;

case ERRO_MQTT:
    display.clear();
    SetBarraStatus(80);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Iniciando MQTT");
    
   delay(500) ; 
   break;

case OK_MQTT:

    display.clear();
    SetBarraStatus(90);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Serviço conectado"); 
    delay(1000); 
    break;

case FIM_INICIALIZACAO:
      display.clear();
      SetBarraStatus(100);
      delay(1000);
      display.drawString(64, 50, "Dispositivo Iniciado"); 
      
      
      break;

  case LENDO_SENSOR: 
    display.clear();
    
    for (int i = 0; i <= 100 ; i += 10)
    {
      SetBarraStatus(i);
      display.setFont(ArialMT_Plain_10);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.drawString(64, 50, "Lendo sensor ...");       
      delay(100);      
      display.display();
    }
    
    break;

  case ENVIANDO_INFO:
    
      display.clear();
       //Desenha as molduras
      display.drawRect(0, 0, 128, 16);
       //Atualiza informacoes da temperatura
      display.setFont(ArialMT_Plain_10);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      String msgMolduraPrincipal = "Monitor da Sala " + String(ID_MQTT) ;
      display.drawString(64, 2, msgMolduraPrincipal.c_str());
      
      display.drawRect(0, 16, 128, 48);
      display.setFont(ArialMT_Plain_24);
      display.drawString(26, 26, String(TemperaturaAtual,1));
      display.drawCircle(52, 32, 2);

      display.drawLine(60, 16, 60, 63);
      display.drawRect(0, 16, 128, 48);
      display.setFont(ArialMT_Plain_24);
      display.drawString(96, 26, String(UmidadeAtual,0) + "%");      


      /*display.drawLine(67, 16, 67, 63);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.setFont(ArialMT_Plain_16);
      display.drawString(96, 20, "Média");      
      display.setFont(ArialMT_Plain_16);
      display.drawLine(67, 40, 128, 40);
      display.drawString(90, 43, String(MediaAtual,1));
      display.drawCircle(107, 45, 2);*/
    
    break;

  

  //default:
  /*  SetLed(Desligado);*/
  //  break;
  } 

  
  display.display();
}

/*********************************************************************************************
 *  Rotina para mostrar a porta serial funcionando e 
 *  as informações do software para o usuario
*********************************************************************************************/

void MsgInicializacao()
{
  delay(2000);
  Serial.println(Separador);
  Serial.println(Firmware);
  Serial.println(Versao);
  Serial.println(Separador);
  delay(3000);
}

/*********************************************************************************************
 * Rotina de conexão e status da conexão
*********************************************************************************************/

void ConectarWiFi()
{
  int contador=1;
  ReportarInfoUsuario(DISCONECTADO);
  Serial.print(F("Conectando na rede WiFi "));
  Serial.print(ssid);
  Serial.print(F(" "));
  Serial.println("...");
  Serial.flush();
  while (statusRede != WL_CONNECTED )
  {    
    Serial.print(".");
    Serial.flush();
    pisca = !pisca;
    statusRede = WiFi.begin(ssid, pass);
    Serial.print("Tentativa ");
    Serial.println(contador);
    contador++;
    Serial.flush();    
    delay(15000);
  }
  Serial.println("Saiu");
  Serial.print(F("Conectado a rede "));
  Serial.println(ssid);
  PrintWiFiConfigs();  
  ReportarInfoUsuario(CONECTADO);
}

/*************************************************************************************
 * Mostra as configurações de rede, da placa e qualidade de sinal
 *************************************************************************************/
void PrintWiFiConfigs()
{
  byte mac[6];
  Serial.println(F("###### WiFi Configs #######"));
  Serial.print(F("IP: "));
  Serial.println(WiFi.localIP());
  Serial.print(F("MAC: "));
  WiFi.macAddress(mac);
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);

  IPAddress subnet = WiFi.subnetMask();
  Serial.print("NetMask: ");
  Serial.println(subnet);

  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("Gateway: ");
  Serial.println(gateway);

  Serial.println();
  Serial.print(F("Rede: "));
  Serial.println(WiFi.SSID());

  Serial.print(F("RSSI: "));
  Serial.println(WiFi.RSSI());
  Serial.println(F("###### WiFi Configs #######"));  
}


/*************************************************************************************
 * Inicializa conexão com MQTT
 *************************************************************************************/
void ConectarMQTT()
{
  IPAddress server(200, 129, 247, 243);
  MQTT.setServer(server, 1883);   //informa qual broker e porta deve ser conectado
  ReportarInfoUsuario(ERRO_MQTT);
  // Loop until we're reconnected
  while (!MQTT.connected()) {
    Serial.print("Tentando conectar ao broker ");
    Serial.println( BROKER_MQTT );
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (MQTT.connect(clientId.c_str())) {
      Serial.println("Conectado");  
      MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)  
      MQTT.subscribe(topicoConectadoTemp.c_str());
      MQTT.subscribe(topicoConectadoUmid.c_str());
    } 
    else {
      Serial.print("Falha, rc=");
      Serial.print(MQTT.state());
      Serial.println(" Tentando novamente em 5 segundos");
      // Wait 5 seconds before retrying
      delay(5000);
    }    
  }
  ReportarInfoUsuario( OK_MQTT );
}


/*************************************************************************************
 *  Callback para chegada de mensagens do broker
 *************************************************************************************/
void mqtt_callback(char* topic, byte* payload, unsigned int length)
{
  String msg;
  Serial.print("Message recebida [");
  Serial.print(topic);
  Serial.print("] ");
  //obtem a string do payload recebido
  for (int i = 0; i < length; i++)
  {
    char c = (char)payload[i];
    msg += c;
  }
  Serial.println(msg);
  TratarMsgRecebida(topic, msg );  
}


void TratarMsgRecebida(String topic, String msg )
{
  if (( topic == topicoConectadoTemp ) && (msg == "não"))
   {
      Serial.println("Nova mensagem publicada Temp");
      // publicando que está ativo      
      MQTT.publish(topicoConectadoTemp.c_str(), "sim");
      ReportarInfoUsuario(ENVIANDO_INFO);
      flagSensorTemp1 = true;
      
      ReportarInfoUsuario(DESLIGAR_LED);
   }

   if ( (topic = topicoConectadoUmid ) && (msg == "não")) 
     {
      Serial.println("Nova mensagem publicada Umid");
      // publicando que está ativo      
      MQTT.publish(topicoConectadoUmid.c_str(), "sim");
      ReportarInfoUsuario(ENVIANDO_INFO);
      flagSensorTemp1 = true;
      
      ReportarInfoUsuario(DESLIGAR_LED);
   }
}



/********************************************************************************************
 *  Inicializando o sensor DS18B20
 ********************************************************************************************/
bool ConectarSensor()
{
  byte i;

  //byte type_s;
  //byte addr[8];  

    if ( !sensorTemp1.search(addr)) {
    Serial.println("No more addresses.");
    Serial.println();
    sensorTemp1.reset_search();
    delay(250);
    return false;
  }

  Serial.print("ROM =");
  for( i = 0; i < 8; i++) {
    Serial.write(' ');
    Serial.print(addr[i], HEX);
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return false;
  }

  Serial.println();

// the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      Serial.println("---------> Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      Serial.println("---------> Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      Serial.println("---------> Chip = DS1822");
      type_s = 0;
      break;
    default:
      Serial.println("Device is not a DS18x20 family device.");
      return false;
  } 
  
  return true;
}

/********************************************************************************************
 *  Lendo dado de temperatura do sensor
 ********************************************************************************************/
float LerInfoSensor()
{
  byte data[12];
  byte present = 0;
  byte i;
  float celsius, fahrenheit;

  sensorTemp1.reset();
  sensorTemp1.select(addr);
  sensorTemp1.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = sensorTemp1.reset();
  sensorTemp1.select(addr);    
  sensorTemp1.write(0xBE);         // Read Scratchpad

  Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = sensorTemp1.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("  Temperature = ");
  Serial.print(celsius);
  Serial.print(" Celsius, ");
  Serial.print(fahrenheit);
  Serial.println(" Fahrenheit");

  return celsius;

}

/**
 * @brief Inicializando o sensor DHT22
 * 
 */
bool ConectarSensorDHT22()
{
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));

  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  return true;
}


/********************************************************************************************
 *  Lendo dado de temperatura do sensor DHT22
 ********************************************************************************************/
void LerTempSensorDHT22( float *t, float *h)
{
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
    *t = 0;
  }
  else {
    Serial.print(F("Temperature: "));
    Serial.print(event.temperature);
    Serial.println(F("°C"));
    *t  = event.temperature;
  }
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)){
        Serial.println(F("Error reading humidity!"));
        *h = 0;
  
  }
  else {
    Serial.print(F("Umidade: "));
    Serial.print(event.relative_humidity);
    Serial.println(F("%"));
    *h = event.relative_humidity;
  }
  
  //return event.temperature;
}



/*************************************************************************************
 * Mostra na serial os comandos disponveis
 *************************************************************************************/
void help()
{
  Serial.println(F("============================================================"));
  Serial.println(F("||                   Help de comandos                     ||"));
  Serial.println(F("============================================================"));
  Serial.println(F("|| set wifissid=<novoSSID>    |  Ajusta e salva novo SSID ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set wifipass=<novoPass>    |  Ajusta e salva nova PASS ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set id=<novoId>            |  Ajusta e salva novo ID   ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| statusWiFi                 |  Mostra configs da rede   ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| help                       |  Mostra help de comandos  ||"));
  Serial.println(F("============================================================"));
}

/*************************************************************************************
 * Funcao para obter os parametros passados pela serial
 *************************************************************************************/
String obterParametroSerial(int nroParam )
{
  String tmp="";
  tmp.reserve(5);
  switch (nroParam)
  {
  case 1:
    // formato esperado @salvar 24.2 60
    tmp = bufferSerial.substring(8,12);
#if _DEBUG >= DEBUG_SERIAL
    Serial.print("par 1 ->");
    Serial.println(tmp.toFloat());
#endif
    break;
  case 2:
    tmp = bufferSerial.substring(13,15);
#if _DEBUG >= DEBUG_SERIAL
    Serial.print("par 2 ->");
    Serial.println(tmp.toInt());
#endif
    break;
  case 3:
    tmp = bufferSerial.substring(12,16);
#if _DEBUG >= DEBUG_SERIAL
    Serial.print("par 3 ->");
    Serial.println(tmp.toInt());
#endif
    break;
  }
  return tmp;
} 

void tratarEntradaSerial()
{
  uint8_t par2;
  float par1;
  String tmpMsg;
  char parametro[30]; 
 
  // Parando timer para tratar informaçoes que estao sendo recebidas
  // -> não precisa.stop();

  // parando a interrupção
  //  MostrarInstanteAtual();
 if (cmdParser.parseCmd(cmdBuffer.getStringFromBuffer()) != CMDPARSER_ERROR) {
  Serial.print("Comando identificado: ");
  Serial.println(cmdParser.getCommand());
  Serial.print("Quantidade de parâmetros: ");
  Serial.println(cmdParser.getParamCount());

  if (cmdParser.equalCommand_P(PSTR("HELP")))
  {
    Serial.println("Ajuda ");
    help();
  }
  
  else if ( cmdParser.equalCommand_P(PSTR("STATUSWIFI")))
  {
    Serial.println(F("Exibindo status da rede WiFi"));
    PrintWiFiConfigs();
    Serial.println();
    Serial.print(F("Tempo desde o ultimo pacote: "));
    Serial.print((millis() - instanteUltimoEnvio)/1000.0);
    Serial.println(F("s"));
  }
  else if (cmdParser.equalCommand_P(PSTR("SET")))
  {
    Serial.println(F("Função de ajuste de configuração"));
    tmpMsg = cmdParser.getValueFromKey("WIFISSID"); 
    Serial.print("tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
    Serial.print(F("Ajustando SSID da Rede sem Fio: "));
    Serial.println(tmpMsg);

    SaveConfigEEPROM("SSID", tmpMsg);
    }

    tmpMsg =  cmdParser.getValueFromKey_P(PSTR("WIFIPASS")); 
    Serial.print("tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
    Serial.print(F("Ajustando Pass da Rede sem Fio: "));
    Serial.println(tmpMsg);
    SaveConfigEEPROM("PASS", tmpMsg);
    }

    Serial.println(cmdParser.getValueFromKey("ID"));
    Serial.print("tmpMsg:");
    tmpMsg = cmdParser.getValueFromKey_P(PSTR("ID"));
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
    Serial.print(F("Ajustando Id do equipmamento: "));
    Serial.println(tmpMsg);
    SaveConfigEEPROM("ID", tmpMsg);
    }
    // implementar salvando na EEPROM
  }
  else if ( cmdParser.equalCommand_P(PSTR("RESET")))
  {
    Serial.print(F("Reset da memória"));
  }
  else 
    Serial.println(F("Comando não reconhecido"));

 }
  cmdBuffer.clear() ;

  /*
  else if ( tmpMsg.equalsIgnoreCase("@dado")) // usuário solicitando coleta de dados 
  {
    Serial.println(F("Ativando flag de captura"));    
    usuarioSolicitaLeitura = true;
    Serial.print("valor de flag: ");
    Serial.println(interruptFlagAlarm);
  }
  else if ( tmpMsg.equalsIgnoreCase("@tirq")) // requisição de instante pelo ESP
  {
    instante = rtc.GetDateTime();
    instanteStr = DateTime2String(instante);
    Serial1.print(instanteStr);
  } 
  else if (tmpMsg.equalsIgnoreCase("@help"))
    help();

  // testando se o comando para sair do modo de comandos
  else if (tmpMsg.equalsIgnoreCase("@exit"))
  {
    Serial.print(F("Saindo do modo de comando"));
  }
  else
  {
    tmpMsg = bufferSerial.substring(0,4);
    if (tmpMsg.equalsIgnoreCase("@now"))
    {
      instante = rtc.GetDateTime();
      instanteStr = DateTime2String(instante );
      Serial.println(instanteStr);
    }
    else if (tmpMsg.equalsIgnoreCase("@deb"))
    {
      Serial.println(F("Situacao do sistema"));
      instante = rtc.GetDateTime();
      instanteStr = DateTime2String(instante );
      Serial.println(instanteStr);
      Serial.print(F("Pulsos do pluviometro: "));
      Serial.println(contadorPulsosPluviometro);
      Serial.print("Umidade do ar (%): ");
      Serial.println(sensorHT.readHumidity());
      Serial.print("Temperatura (º): ");
      Serial.println(sensorHT.readTemperature()); 
    } 
    else 
       Serial.println(F("Comando nao reconhecido"));
  //*/
  // zerando buffer a cada comando
  bufferSerial = "";
}

/*************************************************************************************
 * Funçao para tratar os dados recebidos pela porta serial
 *************************************************************************************/
void serialEvent() {
  /*char inChar;
  //Serial.print("Analisando comando\n");
  while (Serial.available()) {
    // get the new byte:
    inChar = (char)Serial.read();
    // add it to the inputString:
    bufferSerial = bufferSerial + String(inChar);
    // Caractere @ inicia modo de prompt de comandos
    if ( (inChar == 10 ) || (inChar == 13) || (inChar == '#')) {
      Serial.print("bufferSerial: ");
      Serial.println(bufferSerial);
      tratarEntradaSerial();
    }
  }*/
  cmdBuffer.readFromSerial(&Serial,0);
  Serial.print("Comando Lido: ");
  Serial.print(cmdBuffer.getStringFromBuffer());
  bufferSerial = cmdBuffer.getStringFromBuffer();
  tratarEntradaSerial();
}


/********************************************************************************************
 * 
 *  Função para calcular media do display
 * 
 *******************************************************************************************/
float CalculaMedia( float v )
{
  float media = 0.0;
  vetorMedia [ qtdeItensMedia %  TAM_MEDIA ] = v;
  qtdeItensMedia ++; 

  if (qtdeItensMedia > TAM_MEDIA)
  {
    flagTamanhoMedia = true;
  }

  for (int i = 0 ; i < TAM_MEDIA ; i++ )
  {
    media += vetorMedia[i];
  }    
        
  if (flagTamanhoMedia )
    return media/TAM_MEDIA;
  else  
    return media / qtdeItensMedia;
}

/********************************************************************************************
 *  Lendo configuracoes da EEPROM
 ********************************************************************************************/
void LoadConfigsEEPROM()
{
  EEPROM.begin(512);
  int temp = EEPROM.read(EEPROM_ID);
  ID_MQTT = temp;
  Serial.print("Id lido: ");
  Serial.println(ID_MQTT);

  //topicoConectado = "IC_Sala" + String(ID_MQTT) + "_online";
  //topicoTemperatura = "IC_Sala" + String(ID_MQTT) + "_Temp1"; 
  topicoConectadoTemp = "PoP_Sala" + String(ID_MQTT) + "_Temp1_online";
  topicoTemperatura = "PoP_Sala" + String(ID_MQTT) + "_Temp1"; 

  
 // novos topicos de Umidade
 topicoConectadoUmid =  "PoP_Sala" + String(ID_MQTT) + "_Umid1_online";
 topicoUmidade = "PoP_Sala" + String(ID_MQTT) + "_Umid1"; 
  int tamString = EEPROM.read(EEPROM_SSID);
  for (int i = 1; i <= tamString; i++)
  {
    ssid += (char) EEPROM.read(EEPROM_SSID + i);
  }
  Serial.println(pass);

  tamString = EEPROM.read(EEPROM_PASS);
  Serial.print("Tamanho string pass: ");
  Serial.println(tamString);
  for (int i = 1; i <= tamString; i++)
  {
    pass += (char) EEPROM.read(EEPROM_PASS + i);
  }
  Serial.println(ssid);
  EEPROM.end();
}

/********************************************************************************************
 *  Salvando configurações na EEPROM
 ********************************************************************************************/
void SaveConfigEEPROM( String key, String value)
{
  int id;
  EEPROM.begin(512);
  if (key == "ID")
    {
      id = value.toInt();
      Serial.print("Id lido do parâmetro: ");
      Serial.println(id);
      EEPROM.write(EEPROM_ID, id );      
    }
  else if  (key == "SSID")
  {
    EEPROM.write(EEPROM_SSID, value.length() );
    for (int i = 1 ; i <= value.length(); i++ )
      EEPROM.write(EEPROM_SSID+i, (char) value[i-1]);

  }
   else if  (key == "PASS")
  {
    EEPROM.write(EEPROM_PASS, value.length() );
    for (int i = 1 ; i <= value.length(); i++ )
      EEPROM.write(EEPROM_PASS+i, (char) value[i-1]);
      
  } 
  EEPROM.end();
}

/*********************************************************************************************
*********************************************************************************************
*********************************************************************************************    
                      SETUP DO FIRMWARE
*********************************************************************************************
*********************************************************************************************
*********************************************************************************************/

void setup() {
  bool SensorOk ;

  unsigned long instanteInicial;
  unsigned long instanteAtual;


  for (int i = 0 ; i < TAM_MEDIA ; i++ )
      vetorMedia[i] = 0.0;
  // iniciando tela

  display.init();
  display.flipScreenVertically();

  // put your setup code here, to run once:
  Serial.begin(9600);
  // ajustando pinos do led de informação
  /*
  pinMode(PINO_VERMELHO, OUTPUT);
  pinMode(PINO_VERDE, OUTPUT);
  pinMode(PINO_AZUL, OUTPUT);
  */
  ReportarInfoUsuario(INICIANDO);
  MsgInicializacao(); 
  help();
  Serial.println(F("Aguardando comandos da porta serial por 10 segundos")) ;
  instanteInicial = millis();
  instanteAtual = millis();
  cmdParser.setOptKeyValue(true);
  while ((instanteAtual - instanteInicial) < 10000 ) //20000)
  {
    cmdBuffer.readFromSerial(&Serial,5000);
    instanteAtual = millis();
    if (cmdBuffer.getBufferSize() > 0)
    {
      Serial.print("Comando Lido: ");
      Serial.println(cmdBuffer.getStringFromBuffer());
      bufferSerial = cmdBuffer.getStringFromBuffer();
      tratarEntradaSerial();
  }
   }

  
  LoadConfigsEEPROM() ; 
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  Serial.println(F("Iniciando Conexão WiFi"));
  ConectarWiFi();
  ReportarInfoUsuario(ERRO_MQTT);
  Serial.println(F("Iniciando MQTT"));
  Serial.print(F("Topico de verificação online é "));
  Serial.println(topicoConectadoTemp);
  Serial.print(F("Topico de verificação online é "));
  Serial.println(topicoConectadoUmid);
    
  ConectarMQTT();
//  MQTT.publish(topicoTemperatura.c_str(),"30");
  MQTT.publish(topicoConectadoTemp.c_str(), "sim");
  MQTT.publish(topicoConectadoUmid.c_str(), "sim");
  ReportarInfoUsuario(OK_MQTT);

  // configurando o sensor DS18B20
  Serial.println("Configurando sensor de temperatura ");
  do
  {
   // SensorOk = ConectarSensor();
   SensorOk = ConectarSensorDHT22();
  }
  while(!SensorOk);
  Serial.println("Sensor detectado ");
  ReportarInfoUsuario(FIM_INICIALIZACAO);
  //TemperaturaAtual = LerInfoSensor();
  LerTempSensorDHT22(&TemperaturaAtual, &UmidadeAtual);
  MQTT.publish(topicoTemperatura.c_str(), String(TemperaturaAtual,1).c_str());
  MQTT.publish(topicoUmidade.c_str(), String(UmidadeAtual,1).c_str());
  ReportarInfoUsuario( ENVIANDO_INFO );

}



/*********************************************************************************************
*********************************************************************************************
*********************************************************************************************    
                      LOOP DO FIRMWARE
*********************************************************************************************
*********************************************************************************************
*********************************************************************************************/

void loop() {
  // put your main code here, to run repeatedly:
  
  if (!WiFi.isConnected() )
    ConectarWiFi();    
  
  if (!MQTT.connected()) {
    ConectarMQTT();
  }
  MQTT.loop();

  if (flagSensorTemp1 )
  {
    flagSensorTemp1=false;
    //TemperaturaAtual =  LerInfoSensor();
    LerTempSensorDHT22(&TemperaturaAtual, &UmidadeAtual);
    
    MediaAtual = CalculaMedia(TemperaturaAtual);
    MQTT.publish(topicoTemperatura.c_str(), String(TemperaturaAtual,1).c_str());   
    MQTT.publish(topicoUmidade.c_str(), String(UmidadeAtual,1).c_str()) ;
   
  }
}